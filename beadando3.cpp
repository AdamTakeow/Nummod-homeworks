/*
 * beadando3.cpp
 *
 *  Created on: 2014 nov. 28
 *      Author: adamtakeow
 *		E-mail: takeow.adam@gmail.com
 */

/*Nemlineáris egyenletrendszer megoldása csillapított Newton-módszerrel.
A programnak 4 nemlineáris egyenletrendszer gyökeit kell közelíteni.*/

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <iomanip>

int lepes=1; //szamolja hogy 1 feladat PLU felbontasanal hanyadik lepesnel jarunk
int csere=0; //a cseréket számolja
bool szingularis=0;

class Matrix{
public:
	Matrix(){ //default constructor
		n=0;
		m=0;
		p=NULL;
	};
	Matrix(const int row, const int col){ //konstruktor amivel megadhatom hogy mekkora legyen a mátrix (0-kal inicializál)
		n=row;
		m=col;
		p=new double*[n]; //sor
		for(int i=0; i<n; i++){
			p[i] = new double[m]; //sorokban az oszlop
			for(int j=0; j<m; j++){
				p[i][j]=0.0; //inicializálás
			}
		}
	}
	int getSor(void){
		return this->n;
	}
	int getOszlop(void){
		return this->m;
	}
	void print(int v=0){ //ha v==1 paraméterrel hívom meg akkor mátrixos alakban írja ki (ellenőrzés)
		if(v==1){
			for(int i=0; i<this->getSor(); i++){
				for(int j=0; j<this->getOszlop(); j++){
					printf("%.8f ",this->p[i][j]);
				}
				printf("\n");
			}
		}else{ //ha v!=1 paraméterrel hívom meg akkor vektort fog sorban kiíni ahogy a feladat kéri
			for(int i=0; i<this->getSor(); i++){
				for(int j=0; j<this->getOszlop(); j++){
					printf("%.8f ",this->p[i][j]);
				}
			}
		}
	}
	double& operator()(const int row, const int col)const{ //a mátrix megadott sor és oszlop indexén lévő értéket adja vissza
		return( p[row-1][col-1] );
	}
	void swap(int mit, int mire){ //sort cserél a mátrixban
		double* temp=new double[m]; //temporális vektor
		for(int i=0; i<m; i++){ //sima elemcsere
			temp[i]=p[mit-1][i];
			p[mit-1][i]=p[mire-1][i];
			p[mire-1][i]=temp[i];
		}
		csere++; //cseréltünk tehát növeljük
	}
	int oszlop_max(int oszlop){ //visszaadja azt a sorindexet ahol a legnagyobb absértékű elem van a megadott oszopban
		double max=0.0; //ez tárolja a maximális elemet
		int max_index; //ez tárolja a maximális elem sorszámát
		for(int i=lepes-1; i<n; i++){
			if(fabs(p[i][oszlop-1])>fabs(max)){
				max=p[i][oszlop-1];
				max_index=i+1;
			}
		}
		if(fabs(max)<1e-15){ //ha a maximális elemet 0-nak tekintjük
			return -1; //-1et ad vissza
		}else{ //egyébként
			return max_index; //visszatér a sorindexével
		}
	}
	Matrix operator*(const Matrix& b){//működik
		Matrix T = Matrix(this->n, b.m);
		if(this->m == b.n){

			for (int r = 0; r < this->n; r++){
				for (int i = 0; i < b.m; i++){
					for (int c = 0; c < this->m; c++){
						T.p[r][i] += this->p[r][c] * b.p[c][i];
					}
				}
			}
		    // T.print(); //ellenőrzés

		}
		return T;
	}
	Matrix& operator= (const Matrix& a){
		n = a.n;
		m = a.m;
		p = new double*[a.n];
		for (int r = 0; r < a.n; r++){
			p[r] = new double[a.m];
			for (int c = 0; c < a.m; c++){
				p[r][c] = a.p[r][c];
		    }
		}
		return *this;
    }

	~Matrix(){ //destructor
	if (p){
		for(int i=0; i<n; i++){
			delete p[i];
		}
		delete p;
		p=NULL;
	}
	}

private:
	int n; //sor
	int m; //oszlop
	double** p; //ezitten A PÉ
};
/*végtelennorma: ||x||végtelen = max |x(i)|*/
double vegtelennorma(Matrix& a){
	double max=0.0;
	for(int i=1; i<=a.getSor(); i++)
		if(fabs(a(i,1))>max)
			max=fabs(a(i,1));
	return max;
}

//meghatározza a feladatnak megfelelően a minimumot
double min(double szam){
	szam=szam*1.5;
	if(szam <= 1)
		return szam;
	else
		return 1;
}
//meghatározza a feladatnak megfelelően a maximumot
double max(double szam){
	if(szam/2 >= 1e-3)
		return szam/2;
	else
		return -1;
}

//ez a függvény fogja visszaadni a megfelelő vektort
void egyenlet(Matrix& a,const Matrix& x, int szam){ //bemenetként kap egy vektort és egy számot (az egyenlet sorszáma)
	if(szam==1){ //a megfelelő sorszámú egyenlettel számolja ki a vektor értékeit
		a(1,1)=(-1*(x(1,1)*x(1,1)))+(x(3,1))+3;
		a(2,1)=(-1*x(1,1))+(2*(x(2,1)*x(2,1)))-(x(3,1)*x(3,1))-3;
		a(3,1)=(x(2,1))-(3*(x(3,1)*x(3,1)))+2;
	}
	if(szam==3){
		a(1,1)=(-1*(4*x(1,1)))+(cos(2*x(1,1)-x(2,1)))-3;
		a(2,1)=(sin(x(1,1)))-(3*x(2,1))-2;
	}
	if(szam==2){
		a(1,1)=(2*(x(1,1)*x(1,1)))-x(2,1)-1;
		a(2,1)=(-1*(x(1,1)))+(2*(x(2,1)*x(2,1)))-1;
	}
	if(szam==4){
		a(1,1)=(x(1,1)*(x(2,1)*x(2,1)))-(4*(x(1,1)*x(2,1)))+(4*x(1,1))-1;
		a(2,1)=(exp(x(1,1)-1))-x(2,1)+1;
	}
}
//meghatározza a jacobi mátrixot
void jacobi(Matrix& a, const Matrix& x, int szam){ //bemenetként kap egy vektort és egy számot (egyenletrendszer sorszáma)
	if(szam == 1){ //az egyenletrendszernek megfelelően adja vissza a jacobi mátrixot
		a(1,1)=-2*x(1,1);
		a(2,1)=-1;
		a(3,1)=0;
		a(1,2)=0;
		a(2,2)=4*x(2,1);
		a(3,2)=1;
		a(1,3)=1;
		a(2,3)=-2*x(3,1);
		a(3,3)=-6*x(3,1);
	}
	if(szam == 2){
		a(1,1)=4*x(1,1);
		a(2,1)=-1;
		a(1,2)=-1;
		a(2,2)=4*x(2,1);
	}
	if(szam == 3){
		a(1,1)=-4+(-2*sin((2*x(1,1))-x(2,1)));
		a(2,1)=cos(x(1,1));
		a(1,2)=sin((2*x(1,1))-x(2,1));
		a(2,2)=-3;
	}
	if(szam == 4){
		a(1,1)=(x(2,1)*x(2,1))-(4*x(2,1))+4;
		a(2,1)=exp(x(1,1)-1);
		a(1,2)=(2*x(1,1)*x(2,1))-(4*x(1,1));
		a(2,2)=-1;
	}
}

int main(){

	int N; //a megoldandó feladatok száma
	std::cin>>N; //beolvassuk a feladatok számát
	int sorszam; // a megoldandó egyenletrendszer sorszáma

	int maxit; //maximális iterációk száma
	double epszilon; //pontosság

	double t; //csillapítási paraméter

	for( int task=1; task<=N; task++){ //elkezdődik az első feladat
		int szar=0;

		//CHECK std::cout<<"A "<<task<<". feladat megoldása"<<std::endl;

	/*Beolvasások*/

		std::cin>>sorszam; // beolvassuk a sorszámát az egyenletrendszernek
		int meret;
		if (sorszam == 1)
			meret=3;
		else
			meret=2;

		Matrix x = Matrix(meret,1); //x0 kezdővektor
		std::cin>>maxit; //beolvassuk a maxitet
		std::cin>>epszilon; //beolvassuk a pontosságot
		for(int j=1; j<=meret; j++) //beolvassuk az x kezdővektort
			std::cin>>x(j,1);
	/*Beolvasások vége*/



	t=1; //a csillapítási paramétert 1-ra állítjuk

	Matrix f_x = Matrix(meret,1); //ebben lesz az x függvénybeli értéke

	Matrix temporary = Matrix(meret,1);

	egyenlet(temporary,x,sorszam);

	double vegtelen_x = vegtelennorma(temporary);//már most megcsináljuk hogy ne kelljen másolattal dolgozni

	Matrix J = Matrix(meret,meret);

	/*Elindítjuk a k-szerinti forciklust*/
	for(int k=0; k<=maxit; k++)
	{
		//CHECK std::cout<<"A "<<k<<". iteráció a k-s ciklusban"<<std::endl;

		if(k == maxit){
			std::cout<<"maxit"<<std::endl;
			break;
		}

		/*Elkészítjük a P mátrixot*/
			Matrix P = Matrix(meret,meret);

			for(int i=1; i<=meret; i++)
				for(int j=1; j<=meret; j++)
					if(i==j)
						P(i,j)=1.0;

		/*Meghatározzuk a J(x(k)) mártixot és az f(x(k)) vektort*/
		//CHECK std::cout<<"Meghatározzuk a J(x(k)) mártixot és az f(x(k)) vektort"<<std::endl;

		egyenlet(f_x,x,sorszam);

		//CHECK std::cout<<"f(x) : "<<std::endl;
		//CHECK f_x.print(1);

		jacobi(J,x,sorszam);

		//CHECK std::cout<<"A J(k(k))"<<std::endl;
		//CHECK J.print(1);

		/*Megoldjuk a J(x(k)) * delta x = -f(x(k)) lineáris egyenletrendszert*/
		//CHECK std::cout<<"Megoldjuk a J(x(k)) * delta x = -f(x(k)) lineáris egyenletrendszert"<<std::endl;

		/*Kell a J(x(k)) mátrix plu felbontása*/
		//CHECK std::cout<<"Kell a J(x(k)) mátrix plu felbontása"<<std::endl;
			szingularis=0;

			lepes=1; //minden feladatot az első lépéssel kezd
			csere=0; //minden felbontás előtt lenullázza a cseréket
			for(int i=0; i<meret; i++){//akkor is sort cserél ha nem kell swap(1,1) //de már jó
				int r=J.oszlop_max(i+1);//ellenőrizni kell hgoy a főelem nem-e 0
				if(r==-1){ //ha a maximális elemet nullának tekintettük
					szingularis=1;
					break;
				}else if((i+1)!=r){ //ja és ha nem szinguláris
					J.swap(i+1,r); //akkor meg nagyon cseréljük meg a sort a max elem sorindexű sorra
					P.swap(i+1,r); //persze minek P vektor ha kapásból a b vektort is cserélhetem?
				}
				for(int j=i+1; j<meret; j++){
					J(j+1,lepes)=J(j+1,lepes)/J(lepes,lepes); //a főelem alatti elemeket elosztjuk a főelemmel
				}
				for(int j=lepes+1; j<=meret; j++){ //a közbenső elemek számolása a felbontás szerint (előző elem - főelem alatti * főelem melletti)
					for(int k=lepes+1; k<=meret; k++){
						J(j,k)=J(j,k)-(J(lepes,k)*J(j,lepes));
					}
				}
				lepes++; //haladunk a következő lépésre a felbontásban
			}

			//CHECK std::cout<<"A J matrix PLU felbontasa"<<std::endl;
			//CHECK J.print(1);
			//CHECK P.print(1);

			if(fabs(J(meret,meret))<1e-15){ //a felbontás végén ellenőrizzük hogy az utolsó elemet nem-e tekintjük 0-nak
				szingularis=1;
			}

			if(szingularis){//ha a jacobi mátrix szinguláris lenne valamely x(k) esetén
				std::cout<<"szingularis ";
				x.print();
				std::cout<<std::endl;
				break;
			}

			csere=csere/2; //számítási okok miatt a cseréket duplán számolja

			/*A J(x(k)) mátrix plu felbontásának vége*/

			for(int i=1; i<=meret; i++) //-f(x(k))
				f_x(i,1)=f_x(i,1)*-1;

			//CHECK std::cout<<"-f(x)"<<std::endl;
			//CHECK f_x.print(1);

			f_x = P * f_x; // sorcserék a P szerint

			//CHECK std::cout<<"Az f_x a sorcserék utan"<<std::endl;
			//CHECK f_x.print(1);

			/*A J(x(k)) * delta x = -f(x(k)) megoldása*/
			//CHECK std::cout<<"A J(x(k)) * delta x = -f(x(k)) megoldása"<<std::endl;

			Matrix deltax = Matrix(meret,1);

			if(fabs(J(meret,meret))>1e-15 && szingularis==0){ //ha az utolsó elem nem volt szinguláris akkor oldjuk meg az egyenletrendszert
			//és máshol se volt szinguláris

			Matrix ze = Matrix(meret,1); //temporális vektor

			for(int i=1; i<=meret; i++)
				ze(i,1)=0;

			/*Ly=b*//*Lze=xmasolat*/

			for(int i=1; i<=meret; i++){
				double temp=0.0;
				for(int j=1; j<=i-1; j++){
					temp=temp+J(i,j)*ze(j,1);
				}
				ze(i,1)=f_x(i,1)-temp;
			}
			/*Ly=b vége*/

			/*Ux=y*//*Udeltax=ze*/

			for(int i=meret; i>=1; i--){
				double temp=0.0;
				for(int j=1; j<=meret; j++){
					temp=temp+J(i,j)*deltax(j,1);
				}
				deltax(i,1)=(ze(i,1)-temp)/J(i,i);
			}
			}//if vége
			/*Ux=y vége*/

			//CHECK std::cout<<"A delta x"<<std::endl;
			//CHECK deltax.print(1);

			/*A J(x(k)) * delta x = -f(x(k)) megoldásának vége (az egyenletrendszer megoldása deltax)*/

			//ELLENŐRIZVE EDDIG JÓ

			/*t-stratégia (8szor próbáljuk)*/
			//CHECK std::cout<<"t-stratégia (8szor próbáljuk)"<<std::endl;

			Matrix y = Matrix(meret,1);

			int strategy=0;

			Matrix f_y = Matrix(meret,1);

			while (strategy<8){
				strategy++;

				//CHECK std::cout<<"strategy: "<<strategy<<std::endl;

				//CHECK std::cout<<"A t paraméter: "<<t<<std::endl;

				/*y:=x(k)+t*delta x*/
				//CHECK std::cout<<"y:=x(k)+t*delta x"<<std::endl;

				Matrix  deltax_copy = Matrix(meret,1);

				for(int i=1; i<=meret; i++)
					deltax_copy(i,1)=deltax(i,1)*t; //megszorozzuk a deltát t-vel

				//CHECK std::cout<<"deltax a t szorzas utan: "<<std::endl;
				//CHECK deltax_copy.print(1);

				for(int i=1; i<=meret; i++)
					y(i,1)=x(i,1)+deltax_copy(i,1); //meghatározzuk y-t

				//CHECK std::cout<<"y vektor (azaz x(k+1))"<<std::endl;
				//CHECK y.print(1);

				egyenlet(f_y,y,sorszam); //meghatározzuk az y függvénybeli értékét

				/*ha ||f(y)||végtelen < ||f(x(k))||végtelen akkor x(k+1):=y és kilépünk a t-stratégiából*/
				//CHECK std::cout<<"ha ||f(y)||végtelen < ||f(x(k))||végtelen akkor x(k+1):=y és kilépünk a t-stratégiából"<<std::endl;

				//CHECK std::cout<<"Az f_y: "<<std::endl;
				//CHECK f_y.print(1);

				//CHECK std::cout<<"8-asciklusból kilépési feltétel"<<std::endl;
				//CHECK std::cout<<"f_y végtelennormája: "<<vegtelennorma(f_y)<<std::endl;
				//CHECK std::cout<<"f_x végtelennormája: "<<vegtelennorma(f_x)<<std::endl;

				if(vegtelennorma(f_y)<vegtelennorma(f_x)){
					x = y;//itt x az x(k+1)
					break;//kilépés a 8 próbálkozásból
				}else{
					/*ha nem teljseül akkor t=max{t/2, 10^-3}*/
					//CHECK std::cout<<"ha nem teljseül akkor t=max{t/2, 10^-3}"<<std::endl;

					t=max(t);

					/*ha t!=10^-3 akkor ugrás a t-stratégia elejére és kezdődik előről*/

					if(t!=-1 && strategy!=8){
						//CHECK std::cout<<"ha t!=10^-3 akkor ugrás a t-stratégia elejére és kezdődik előről"<<std::endl;

						continue;
					}else{
					/*ha t=10^-3 akkor kilépünk hibával*/
						//CHECK std::cout<<"ha t=10^-3 akkor kilépünk hibával"<<std::endl;

						std::cout<<"sikertelen ";
						x.print();
						std::cout<<std::endl;
						szar=1;
						break;
					}
				}

			}/*t-stratégia vége*/
			if(szar){
				break;
			}

			/*Ha x(k)-ból 1. próbálkozásra sikerült meghatározni x(k+1)-t akkor t:=min{1,5*t,1}*/
			//CHECK std::cout<<"Ha x(k)-ból 1. próbálkozásra sikerült meghatározni x(k+1)-t akkor t:=min{1,5*t,1}"<<std::endl;
			if(strategy == 1){
				t=min(t);
			}


			/*Leállási feltétel Ha
			 ||f(x(k+1))||végtelen < epszilon * (1+||f(x(0))||végtelen) teljesül akkor leállás*/
			//CHECK std::cout<<"Ha ||f(x(k+1))||végtelen < epszilon * (1+||f(x(0))||végtelen) teljesül akkor leállás"<<std::endl;

			//CHECK std::cout<<"leállási feltétel"<<std::endl;
			//CHECK std::cout<<"f_y (x(k+1)) végtelennormája: "<<vegtelennorma(f_y)<<std::endl;
			//CHECK std::cout<<"maga a vegtelen_x: "<<vegtelen_x<<std::endl;
			//CHECK std::cout<<"epszilon * (1+x0 végtelennormája): "<<epszilon * (1+vegtelen_x)<<std::endl;

			if(vegtelennorma(f_y) < epszilon * (1+vegtelen_x)){ //f_y-ba van az x(k+1) függvénybeli értéke
				/*leállás*/
				std::cout<<"siker ";
				x.print();
				std::cout<<std::fixed<<std::setprecision(8)<<vegtelennorma(f_y)<<" "<<k<<std::endl; //k+1 de ac szal leszarom
				break;
			}else{
			/*ha nem teljesül akkor folytatjuk a k-szerinti forciklust*/
				//CHECK std::cout<<"ha nem teljesül akkor folytatjuk a k-szerinti forciklust"<<std::endl;
				continue; //k-szerinti folytatása
			}

		}//k-szerinti forciklus vége

	}//feladatmegoldások vége
return 0;
}
//a lényeg az hogy először kiszámoljuk az f(x(k))-t meg a J(x(k))-t és ezekkel megoldjuk az egyenletrendszert
//
