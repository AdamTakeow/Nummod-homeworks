/*
 * beadando2.cpp
 *
 *  Created on: 2014 okt. 31
 *      Author: Adam Bertalan
 *		E-mail: takeow.adam@gmail.com
 */

/*
Inverz iteráció eltolással.
Az A ∈ R mátrix saját ́ertékeit, sajátvektorait keressük. Annak  ́erdekében, hogy
a program az  ̈osszes saját ́erték megkeresésére alkalmas legyen az iterációt az A − cE
mátrixra alkalmazzuk, ahol c ∈ R adott konstans, E ∈ R n×n az egységmátrix. Az iterációt
egy adott y0 kezdővektorból indítva addig folytatjuk, míg a sajátérték két egymás utáni
közelítésének eltérése egy adott korlát alá nem kerül, vagy el nem  ́erjük a maximális
iterációszámot (maxit). Ha a leállási feltétel teljesül, akkor még ellenőrizni kell, hogy
valóban sajátpár közelítését adta-e az iteráció.
*/

#include <iostream>
#include <stdio.h>
#include <math.h>

int lepes=1; //szamolja hogy 1 feladat PLU felbontasanal hanyadik lepesnel jarunk
int csere=0; //a cseréket számolja
bool szingularis=0;

class Matrix{
public:
	Matrix(){ //default constructor
		n=0;
		m=0;
		p=NULL;
	};
	Matrix(const int row, const int col){ //konstruktor amivel megadhatom hogy mekkora legyen a mátrix (0-kal inicializál)
		n=row;
		m=col;
		p=new double*[n]; //sor
		for(int i=0; i<n; i++){
			p[i] = new double[m]; //sorokban az oszlop
			for(int j=0; j<m; j++){
				p[i][j]=0.0; //inicializálás
			}
		}
	}
	int getSor(void){
		return this->n;
	}
	int getOszlop(void){
		return this->m;
	}
	void print(int v=0){ //ha v==1 paraméterrel hívom meg akkor mátrixos alakban írja ki (ellenőrzés)
		if(v==1){
			for(int i=0; i<n; i++){
				for(int j=0; j<m; j++){
					printf("%.8f ",p[i][j]);
				}
				printf("\n");
			}
		}else{ //ha v!=1 paraméterrel hívom meg akkor vektort fog sorban kiíni ahogy a feladat kéri
			for(int i=0; i<n; i++){
				for(int j=0; j<m; j++){
					printf("%.8f ",p[i][j]);
				}
			}
		}
	}
	double& operator()(const int row, const int col){ //a mátrix megadott sor és oszlop indexén lévő értéket adja vissza
		return( p[row-1][col-1] );
	}
	void swap(int mit, int mire){ //sort cserél a mátrixban
		double* temp=new double[m]; //temporális vektor
		for(int i=0; i<m; i++){ //sima elemcsere
			temp[i]=p[mit-1][i];
			p[mit-1][i]=p[mire-1][i];
			p[mire-1][i]=temp[i];
		}
		csere++; //cseréltünk tehát növeljük
	}
	int oszlop_max(int oszlop){ //visszaadja azt a sorindexet ahol a legnagyobb absértékű elem van a megadott oszopban
		double max=0.0; //ez tárolja a maximális elemet
		int max_index; //ez tárolja a maximális elem sorszámát
		for(int i=lepes-1; i<n; i++){
			if(fabs(p[i][oszlop-1])>fabs(max)){
				max=p[i][oszlop-1];
				max_index=i+1;
			}
		}
		if(fabs(max)<1e-15){ //ha a maximális elemet 0-nak tekintjük
			return -1; //-1et ad vissza
		}else{ //egyébként
			return max_index; //visszatér a sorindexével
		}
	}
	friend Matrix operator*(const Matrix& a ,const Matrix& b){
		if(a.m == b.n){
			Matrix T = Matrix(a.n, b.m);
			for (int r = 0; r < a.n; r++){
				for (int i = 0; i < b.m; i++){
					for (int c = 0; c < a.m; c++){
						T.p[r][i] += a.p[r][c] * b.p[c][i];
					}
				}
			}
		    // T.print(1);
			return T;
		}
		return Matrix();
	}
	Matrix& operator= (const Matrix& a){
		n = a.n;
		m = a.m;
		p = new double*[a.n];
		for (int r = 0; r < a.n; r++){
			p[r] = new double[a.m];
			for (int c = 0; c < a.m; c++){
				p[r][c] = a.p[r][c];
		    }
		}
		return *this;
    }
	~Matrix(){ //destructor
		for(int i=0; i<n; i++){
			delete p[i];
		}
		delete p;
		p=NULL;
	}

private:
	int n; //sor
	int m; //oszlop
	double** p; //ezitten A PÉ
};

int main(){
	int maxit; //maximális iterációk száma
	int hany; //ez jelöli a mátrixok számát, ennyiszer fog lefutni a program
	int meret; //a mátrix métere;
	int mfeladat; //minden mátrixhoz mfeladat számú sorpár tartozik!
	double epszilon; //epszilon leállási paraméter
	double c; //az eltolás

	/*****************1. lépés elkészítjük az A-cE mátrix PLU felbontását!*******************/

	/*Beolvasások*/

	//először beolvassuk hogy hány mátrix lesz
	std::cin>>hany;

	//itt rögtön indul is egy for
	for(int mukodj=1; mukodj<=hany;	mukodj++)
	{
		//beolvassuk hogy hányszor hanyas a mátrix
		std::cin>>meret;

		//beolvassuk a mátrixot
		Matrix M = Matrix (meret, meret);

		for(int i=1; i<=meret; i++)
				for(int j=1; j<=meret; j++)
					std::cin >> M(i,j); //beolvasom a mátrix elemeit

		Matrix y = Matrix(meret, 1); //az y kezdővektor

		//beolvassuk az A mátrixra vonatkozó feladatok számát
		std::cin>>mfeladat;

		for(int fos=1; fos<=mfeladat; fos++) //elindul az mfealdat megoldása
		{
		szingularis=0;

		//beolvassuk a c eltolást
		std::cin>>c;

		//beolvassuk a max iterációkat
		std::cin>>maxit;

		//beolvassuk az epszilon leállási feltételt
		std::cin>>epszilon;

		//beolvassuk az y kezdővektort
		for (int i=1; i<=meret; i++)
			std::cin>>y(i,1);

		//készítsünk egy egyésmátrixot!

		Matrix P = Matrix(meret, meret);

		for (int i=1; i<=meret; i++)
			for (int j=1; j<=meret; j++)
				if(i==j)
					P(i,j)=1;

		//mielőtt elkezdenénk az mfeladat feladatok megoldását, el kell tárolni az eredeti
		// beolvasott mátrixot mert az A-cE a c miatt mindig más és más lesz!

		Matrix A = Matrix(meret, meret);
		for(int i=1; i<=meret; i++)
			for(int j=1; j<=meret; j++)
				A(i,j)=M(i,j);

		Matrix C = Matrix(meret, meret);
		for(int i=1; i<=meret; i++)
			for(int j=1; j<=meret; j++)
				C(i,j)=M(i,j);

		//tehát az M a feladatok megoldása előtt beolvasott mátrix, az A pedig az amivel
		//mfeladat-szor számolunk

		/*A-cE*/

		for (int i=1; i<=meret; i++)
			for (int j=1; j<=meret; j++)
			{
				if(i==j){
					A(i,j)=A(i,j)-c;
				}
			}

		/*A-cE vége*/

		/*PLU felbontas*/

		lepes=1; //minden feladatot az első lépéssel kezd
		csere=0; //minden felbontás előtt lenullázza a cseréket

		for(int i=0; i<meret; i++){//akkor is sort cserél ha nem kell swap(1,1) //de már jó
			int r=A.oszlop_max(i+1);//ellenőrizni kell hgoy a főelem nem-e 0
			if(r==-1){ //ha a maximális elemet nullának tekintettük
				szingularis=1;
				//break;//MACSKA //kilépés az mfeladatból
			}else if((i+1)!=r){ //ja és ha nem szinguláris
				A.swap(i+1,r); //akkor meg nagyon cseréljük meg a sort a max elem sorindexű sorra
				P.swap(i+1,r); //persze minek P vektor ha kapásból a b vektort is cserélhetem?
			}
			for(int j=i+1; j<meret; j++){
				A(j+1,lepes)=A(j+1,lepes)/A(lepes,lepes); //a főelem alatti elemeket elosztjuk a főelemmel
			}
			for(int j=lepes+1; j<=meret; j++){ //a közbenső elemek számolása a felbontás szerint (előző elem - főelem alatti * főelem melletti)
				for(int k=lepes+1; k<=meret; k++){
					A(j,k)=A(j,k)-(A(lepes,k)*A(j,lepes));
				}
			}
			lepes++; //haladunk a következő lépésre a felbontásban
		}

	/*		std::cout<<"Az LU felbontás utána az A mátrix"<<std::endl;
			A.print(1);
			std::cout<<std::endl;

			std::cout<<"Az LU felbontás utána a P mátrix"<<std::endl;
						P.print(1);
						std::cout<<std::endl;
*/
		if(fabs(A(meret,meret))<1e-15){ //a felbontás végén ellenőrizzük hogy az utolsó elemet nem-e tekintjük 0-nak
			szingularis=1;
		}

		if(szingularis)//ha szinguláris
		{
			printf("%.8f \n",c);//akkor a c sajátérték, írjuk ki
			continue;//break;//kilépés az mfeladatból //TEMPORARY
		}

		/*PLU felbontas vege*/
/*
		 std::cout<<"Az A mátrix a felbontás után"<<std::endl;
		 A.print(1);

		 std::cout<<"A P mátrix a felbontás után"<<std::endl;
		 P.print(1);
*/
	/*************************2. lépés ||y0||2 kiszámítása************************/

		//kiszámítjuk az y vektor második normáját
		double temp=0.0;

		for (int k=1; k<=meret; k++)
			temp = temp + y(k,1) * y(k,1);

		temp = sqrt(temp);

		if(fabs(temp)<1e-15)
		{
			//itt jön az hogy mivan ha a kezdővektor 0 vektor
			std::cout<<"kezdovektor"<<std::endl;
			continue;//break; //TEMPORARY
		}

	/*************************3. lépés az y normálása******************************/

		//az y minden elemét leosztom a saját normájával
		for (int k=1; k<=meret; k++)
			y(k,1)=y(k,1)/temp;
		//	std::cout<<"az y vektor normálás után"<<std::endl;
		//	y.print();

		//az 1. lambda az a (Ay,y) Rayleigh hányados

		//C*y
		Matrix T = Matrix(C.getSor(), y.getOszlop());

			//létrehozunk egy eredménymátrixot megfelelő mérettel
			//Az A mátrix a felbontás utáni mátrix, M a felbontás előtti
			//A C mátrix az M mátrix, ezzel kell számolni a rayleigh hányadost

		temp=0.0;
		for(int i=1; i<=C.getOszlop(); i++)
		{
			temp=0.0;
			for(int j=1; j<=y.getSor(); j++)
			{
				temp=temp+C(i,j)*y(j,y.getOszlop()); //összeszorozzuk a C-t az y-nal sor oszlop kompozícióban
			}
			T(i,T.getOszlop())=temp;
		}
		//T egy oszlopvektor amiben C*y van

		//ide jön a belső szorzat

		//(T,y)
		temp=0.0;
		for(int i=1; i<=meret; i++)
			temp+=T(i,1)*y(i,1); //tempben lesz az első sajátérték

		double lambda1 = temp;

		double lambda2 = 0.0;

	/****************4. lépés elindul az iteráció*************/

		Matrix yp = Matrix(P.getSor(), y.getOszlop());
		Matrix Z = Matrix(A.getSor(), yp.getOszlop()); //ebben lesz az iteráción belüli C*y
		//létrehozunk egy eredménymátrixot megfelelő mérettel

		for(int k=1; k<=maxit; k++)
		{
		//itt a maxit ciklus elején az előző maxit ciklus x-vektorára kell végrehajtani a permutációt
		//!!!!!!!!!!!!!!
		/*elvégezzük az y vektoron a permutációt*/

		//P*y

			Matrix x = Matrix(meret,1); //és egy x vektorom

				temp=0.0;
				for(int i=1; i<=P.getOszlop(); i++)
				{
					temp=0.0;
					for(int j=1; j<=x.getSor(); j++)
					{
						temp=temp+P(i,j)*y(j,y.getOszlop());
					}
					yp(i,yp.getOszlop())=temp;
				}

			/*elvégezzük a két visszahelyettesítést Lz=yp, Ux=z*/

			Matrix z = Matrix(meret,1); //lesz egy z vektorom

			/*Lz=yp*/

			for(int i=1; i<=meret; i++){
				double temp=0.0;
				for(int j=1; j<=i-1; j++){
					temp=temp+A(i,j)*z(j,1);
				}
				z(i,1)=yp(i,1)-temp;
			}
			//std::cout<<"yvektor"<<std::endl;
			//z.print();

			/*Lz=yp vége*/

			/*Ux=z*/

			for(int i=meret; i>=1; i--){
				double temp=0.0;
				for(int j=1; j<=meret; j++){
					temp=temp+A(i,j)*x(j,1);
				}
				x(i,1)=(z(i,1)-temp)/A(i,i);
			}

			/*Ux=z vége*/

		/*a kapott x vektort normáljuk: x=x/||x||2*/

			//kettőnorma
			temp=0.0;
			for (int i=1; i<=meret; i++)
				temp+=x(i,1) * x(i,1);

			temp = sqrt(temp);

			//normálás
			for (int i=1; i<=meret; i++)
				y(i,1)=x(i,1)/temp;

		/*kiszámítjuk a sajátérték közelítést lambda(k) = (Ax,x)*/

		//C*y

			Matrix Z = Matrix(C.getSor(), y.getOszlop());//létrehozunk egy eredménymátrixot megfelelő mérettel
			temp=0.0;
			for(int i=1; i<=C.getOszlop(); i++)
			{
				temp=0.0;
				for(int j=1; j<=y.getSor(); j++)
				{
					temp=temp+C(i,j)*y(j,y.getOszlop());
				}
				Z(i,Z.getOszlop())=temp;
			}
			//Z egy oszlopvektor

			//ide jön a belső szorzat
			//(Z,yp)
			temp=0.0;
			for(int i=1; i<=meret; i++)
				temp+=Z(i,1)*y(i,1);

			lambda2 = temp; //a második sajátérték

		/*ellenőrzés a leállási feltétellel lambda(k) - lambda(k-1) abszolútértékben kisebb-e mint
		 * epszilon*(1+|lambda(k)|*/


			if(fabs(lambda2-lambda1)<=(epszilon*(1+fabs(lambda2))))
			{
			/*sikerteszt*/
			//ellenőrizzük hogy valóban sajátértékpárt kaptunk-e
			//||Ay-lambda[k]y||kettőnorma négyzet <= epszilon

//				std::cout<<fabs(lambda2-lambda1)<<" <= "<< epszilon*(1+fabs(lambda2))<<std::endl;

				Matrix tmp = Matrix(meret,1);

				for (int i=1; i<=meret; i++)
					tmp(i,1)=y(i,1)*lambda2;
				for (int i=1; i<=meret; i++)
					Z(i,1)=Z(i,1)-tmp(i,1);

				//Z-nek kell a kettőnorma négyzete

				temp=0.0;
				for (int i=1; i<=meret; i++)
					temp+=Z(i,1) * Z(i,1);

				temp = sqrt(temp);

				//és a négyzete

				temp=temp*temp;

				if(temp<=epszilon){
				//	std::cout<<"siker "<<lambda2<<" ";
					printf("siker %.8f ",lambda2);
					y.print();
				//	std::cout<<" "<<temp<<" "<<k-1<<std::endl;
					printf(" %.8f %d \n", temp, k-1);
					break;
				}else{
				//	std::cout<<"sikertelen "<<lambda2<<" ";
					printf("sikertelen %.8f ",lambda2);
					y.print();
				//	std::cout<<" "<<temp<<" "<<k-1<<std::endl;
					printf(" %.8f %d \n", temp, k-1);
					break;
				}

				//akkor sikeres leállás
				//egyébként sikertelen leállás
			}//ifvege


			if (k==maxit)
			{
			//elértük a maxitet
				std::cout<<"maxit"<<std::endl;
				break; //TEMPORARY
			}

			lambda1=lambda2; //a végén az új lambdát beletesszük a régibe és a következő iterációban meghatározzuk a lambda2 új értékét

		}//for maxit vége

	}//az mfeladat for vége

}//matrixos feladatok vége
return 0;
}
