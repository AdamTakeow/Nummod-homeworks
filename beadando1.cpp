/*
 * beadando1.cpp
 *
 *  Created on: 2014 okt. 10
 *      Author: Bertalan Ádám
 *		E-mail: takeow.adam@gmail.com
 */

/*
 *Numerikus módszerekből az első programozandó algoritmus a PLU-felbontás.
 *A program beadási határideje október 27., 12 óra.
 *Az output lebegőpontos számait 8 (nyolc) tizedes jegy pontossággal irjátok ki.
 *Fokozottan ügyeljetek lebegőpontos számok egyenlőségének vizsgálatára!!!
 *( a == b helyett |a - b| < valami ami eleg kicsi...)
 *A programoknak a szabványos bemenetről kell olvasniuk az adatokat illetve a
 *szabványos kimenetre kell írniuk az eredményt. A programoknak 3 másodpercen
 *belül kell a megoldást produkálniuk.
 */

// v2.1

#include <iostream>
#include <stdio.h>
#include <math.h>

int lepes=1; //szamolja hogy 1 feladat PLU felbontasanal hanyadik lepesnel jarunk
int csere=0; //a cseréket számolja
bool szingularis=0;

class Matrix{
public:
	Matrix(){ //default constructor
		n=0;
		m=0;
		p=NULL;
	};
	Matrix(const int row, const int col){ //konstruktor amivel megadhatom hogy mekkora legyen a mátrix (0-kal inicializál)
		n=row;
		m=col;
		p=new double*[n]; //sor
		for(int i=0; i<n; i++){
			p[i] = new double[n]; //sorokban az oszlop
			for(int j=0; j<m; j++){
				p[i][j]=0.0; //inicializálás
			}
		}
	}
	void print(int v=0){ //ha v==1 paraméterrel hívom meg akkor mátrixos alakban írja ki (ellenőrzés)
		if(v==1){
			for(int i=0; i<n; i++){
				for(int j=0; j<m; j++){
					printf("%.8f ",p[i][j]);
				}
				printf("\n");
			}
		}else{ //ha v!=1 paraméterrel hívom meg akkor vektort fog sorban kiíni ahogy a feladat kéri
			for(int i=0; i<n; i++){
				for(int j=0; j<m; j++){
					printf("%.8f ",p[i][j]);
				}
			}
		}
	}
	double& operator()(const int row, const int col){ //a mátrix megadott sor és oszlop indexén lévő értéket adja vissza
		return( p[row-1][col-1] );
	}
	void swap(int mit, int mire){ //sort cserél a mátrixban
		double* temp=new double[m]; //temporális vektor
		for(int i=0; i<m; i++){ //sima elemcsere
			temp[i]=p[mit-1][i];
			p[mit-1][i]=p[mire-1][i];
			p[mire-1][i]=temp[i];
		}
		csere++; //cseréltünk tehát növeljük
	}
	int oszlop_max(int oszlop){ //visszaadja azt a sorindexet ahol a legnagyobb absértékű elem van a megadott oszopban
		double max=0.0; //ez tárolja a maximális elemet
		int max_index; //ez tárolja a maximális elem sorszámát
		for(int i=lepes-1; i<n; i++){
			if(fabs(p[i][oszlop-1])>fabs(max)){
				max=p[i][oszlop-1];
				max_index=i+1;
			}
		}
		if(fabs(max)<1e-15){ //ha a maximális elemet 0-nak tekintjük
			return -1; //-1et ad vissza
		}else{ //egyébként
			return max_index; //visszatér a sorindexével
		}
	}
	~Matrix(){ //destructor
		for(int i=0; i<n; i++){
			delete p[i];
		}
		delete p;
		p=NULL;
	}

private:
	int n; //sor
	int m; //oszlop
	double** p; //ezitten A PÉ
};

int main(){

	int feladat; //ez tárolja hogy hány feladatot kell megoldanunk
	std::cin>>feladat; //be is kéne olvasni
	int meret;
	int szam;

	double** f;//eredménymátrix
	//tudom hogy hány sora lesz mert annyi sora lesz ahány feladat, így ezt már lefoglalhatom

	f=new double*[feladat];//pl 7 feladat esetén az f egy 7 soros vektortömb
	//az hogy hány oszlopa lesz az egyes soroknak mindig a meret beolvasása után derül ki (meret+1)

	for(szam=0; szam<feladat; szam++){//számolja hogy hanyadik feladatnál járunk

		szingularis=0; //minden feladat elején a mátrix nem szinguláris

		/*beolvasas*/

		double det=1.0; //a mátrix determinánsa kezdetben 1 csak mer'
		std::cin>>meret; //be is olvasom
		f[szam]= new double[meret+2]; //itt már tudom hogy a sorok milyen hosszúak lesznek +2 hely a determináns és a sorszám miatt

		Matrix b = Matrix(meret,1); //a b vektorom "meret" sorral és 1 oszloppal
		Matrix M = Matrix(meret, meret); //a mátrix "meret" sorral és oszloppal

		for(int i=1; i<=meret; i++){
			for(int j=1; j<=meret; j++){
				std::cin >> M(i,j); //beolvasom a mátrix elemeit
			}
		}
		for(int i=1; i<=meret; i++){ //beolvassuk a b vektort
			std::cin>>b(i,1); //beolvasom a b vektor elemeit
		}

		/*beolvasas vege*/

		/*LU felbontas*/

		lepes=1; //minden feladatot az első lépéssel kezd
		csere=0; //minden felbontás előtt lenullázza a cseréket
		for(int i=0; i<meret; i++)
		{//akkor is sort cserél ha nem kell swap(1,1) //de már jó
			int r=M.oszlop_max(i+1);//ellenőrizni kell hgoy a főelem nem-e 0
			if(r==-1)
			{ //ha a maximális elemet nullának tekintettük
				szingularis=1;
				break;
			}
			else if((i+1)!=r)
			{ //ja és ha nem szinguláris
				M.swap(i+1,r); //akkor meg nagyon cseréljük meg a sort a max elem sorindexű sorra
				b.swap(i+1,r); //persze minek P vektor ha kapásból a b vektort is cserélhetem?
			}
			for(int j=i+1; j<meret; j++)
			{
				M(j+1,lepes)=M(j+1,lepes)/M(lepes,lepes); //a főelem alatti elemeket elosztjuk a főelemmel
			}
			for(int j=lepes+1; j<=meret; j++)
			{ //a közbenső elemek számolása a felbontás szerint (előző elem - főelem alatti * főelem melletti)
				for(int k=lepes+1; k<=meret; k++)
				{
					M(j,k)=M(j,k)-(M(lepes,k)*M(j,lepes));
				}
			}
			lepes++; //haladunk a következő lépésre a felbontásban
		}
		std::cout<<"ellenorzes: "<<std::endl;
		M.print(1);

		if(fabs(M(meret,meret))<1e-15){ //a felbontás végén ellenőrizzük hogy az utolsó elemet nem-e tekintjük 0-nak
			szingularis=1;
		}

		/*LU felbontas vege*/

		csere=csere/2; //számítási okok miatt a cseréket duplán számolja

		Matrix y = Matrix(meret,1); //lesz egy y vektorom
		Matrix x = Matrix(meret,1); //és egy x vektorom

		if(fabs(M(meret,meret))>1e-15 && szingularis==0){ //ha az utolsó elem nem volt szinguláris akkor oldjuk meg az egyenletrendszert
//és máshol se volt szinguláris
			/*Ly=b*/

			for(int i=1; i<=meret; i++){
				double temp=0.0;
				for(int j=1; j<=i-1; j++){
					temp=temp+M(i,j)*y(j,1);
				}
				y(i,1)=b(i,1)-temp;
			}
			std::cout<<"yvektor"<<std::endl;
			y.print();
			/*Ly=b vége*/

			/*Ux=y*/

			for(int i=meret; i>=1; i--){
				double temp=0.0;
				for(int j=1; j<=meret; j++){
					temp=temp+M(i,j)*x(j,1);
				}
				x(i,1)=(y(i,1)-temp)/M(i,i);
			}

			/*Ux=y vége*/

			/*determináns számolás*/

			for(int i=1; i<=meret; i++){
				for(int j=1; j<=meret; j++){
					if(i==j){
						det*=M(i,j);
					}
				}
			}
			if(csere%2!=0){
				det=det*-1;
			}

			/*determináns számolás vége*/

		} //egyenletrendszer megoldásának vége

		if(szingularis==1){//ha szinguláris volt akkor nem számol determinánst! Ezért kell egy ilyen
			det=0;
		}

			//még a feladaton belül belerakom az egyes vektorokba az eredményt
			f[szam][0]=meret; //az első eleme maga a vektor mérete
			f[szam][1]=det; //a második eleme a determináns
			int j=1;
			for(int i=2; i<meret+2; i++){
				f[szam][i]=x(j,1); //az f vektor elemei az x vektor elemei lesznek
				++j;
			}

		}
	//feladaton kívüli rész
	//ide kell kiírjam az f mátrixot amihez tudnom kell az egyes sorok hosszát //ez az egyes sorok első eleme

	/*kiíratás*/

	printf("\n");
	for(int i=0; i<feladat; i++){
		for(int j=1; j<f[i][0]+2; j++){
			if(fabs(f[i][1])==0){
				std::cout<<"szingularis";
				break;
			}else{
				printf("%.8f ",f[i][j]);
			}
		}
		printf("\n");
	}

	/*kiíratás vége*/

	return 0;
}
